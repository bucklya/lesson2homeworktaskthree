package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Программа предназначенна для подсчета букв, слов и предложений в тексте");
        System.out.println("Введите ваш текст: ");
        Scanner importText = new Scanner(System.in);
        String text = importText.nextLine();

        // System.out.println(text);

        int countingLetters = text.length();
        int countingWords = 0;
        int countingSentence = 0;
        for (int i = 0; i < text.length(); i++) {
            char symbol = text.charAt(i);
            if (symbol == ' ' || symbol == '.' || symbol == ',' || symbol == '!' || symbol == '?' || symbol == ':'
                    || symbol == ';' || symbol == '-' || symbol == '—') {
                countingLetters = countingLetters - 1;
                countingWords += 1;
                if (symbol == '.' || symbol == '!' || symbol == '?' || symbol == ';') {
                    countingSentence += 1;
                }
                if (i < text.length() - 1) {
                    int j = i + 1;
                    char symbolNext = text.charAt(j);
                    if (symbolNext == ' ' || symbolNext == '-' || symbolNext == '—') {
                        countingWords -= 1;
                    }
                }
            }
        }
        System.out.println("Количество букв в тексте = " + countingLetters);
        System.out.println("Количество слов в тексте = " + countingWords);
        System.out.println("Количество предложений в тексте = " + countingSentence);
    }
}
